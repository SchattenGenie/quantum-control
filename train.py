from comet_ml import Experiment
    
experiment = Experiment(api_key="lODeHEtCf7XLaV6DJrOfugNcA",
                        project_name="quantum-control", workspace="holybayes")
import sys
import torch
import numpy as np
sys.path.append('../')
from env import QuantumControl
from utils.optim import Ranger
import json
import os

CKPT_PATH = 'best.ckpt'

env_params = {'max_stamp': 6, 'batch_size': 1}
amp = 1e-2
relaxation_steps = 5 # number of steps to wait until system relaxed
epoches = 1000
lr = 1e-3

qc = QuantumControl(**env_params)
env_params['inverse'] = True
qc_inv = QuantumControl(**env_params)


baseline = np.arctan(np.array([[-0.0018216546513159368, 0.0021649111307008267], 
                      [0.00295401791696428, -0.0004872600190229271], 
                      [-4.7152522857042517e-07, 0.0006462490557820025], 
                      [-0.001929144421123893, 0.002999860191281257]]).astype('float64'))/amp

# baseline = 2*(np.random.random((4,2))-0.5)

def step(action):
    act = amp*torch.tanh(action)
    qc.step(act)
    qc_inv.step(act)
    
def reset():
    qc.reset()
    qc_inv.reset()
    
def eval_loss():
    loss = ((qc.qubit_target - qc.qubit_state[:,:,:,0])**2).sum()
    loss_inv = ((qc_inv.qubit_target - qc_inv.qubit_state[:,:,:,0])**2).sum()
    return (loss + loss_inv)/2

def eval_score():
    return (qc.fidelity.squeeze().item()+qc_inv.fidelity.squeeze().item())/2

actions = [torch.from_numpy(act) for act in baseline]
for act in actions: act.requires_grad=True

opt = Ranger(actions, lr=lr)
best_score = 0

if os.path.isfile(CKPT_PATH):
    print(f'Loading from {CKPT_PATH}')
    state_dict = torch.load(CKPT_PATH)
    actions = state_dict['actions']
    opt = Ranger(actions, lr=lr)
    best_score = state_dict['score']
    opt.load_state_dict(state_dict['opt'])
    
for epoch in range(epoches):
    for action in actions:
        step(action)
    for action in np.zeros((relaxation_steps,2)):
        step(torch.from_numpy(action))
    loss = eval_loss()
    loss.backward()
    opt.step()
    opt.zero_grad()
    score = eval_score()
    reset()
    if score > best_score: 
        print(f'Best score updated: {best_score} -> {score}')
        best_score = score
        state_dict = {'Loss': loss.item(), 'opt': opt.state_dict(), 'score': score, 'actions': actions}
        torch.save(state_dict, 'best.ckpt')
    experiment.log_metrics({'metric': score, 'loss': loss.item(), 'best': best_score}, step=epoch)
