import numpy as np
import scipy.signal as signal
import torch
import gc

class IIR2Filter(torch.nn.Module):           
    
    def createCoeffs(self,order=3,cutoff=0.9,filterType='lowpass',design='bessel',rp=1,rs=1,fs=1):
        
        #defining the acceptable inputs for the design and filterType params
        self.designs = ['butter','cheby1','cheby2','bessel']
        self.filterTypes1 = ['lowpass','highpass','Lowpass','Highpass','low','high']
        self.filterTypes2 = ['bandstop','bandpass','Bandstop','Bandpass']
        
        #Error handling: other errors can arise too, but those are dealt with 
        #in the signal package.
        self.isThereAnError = 1 #if there was no error then it will be set to 0
        self.COEFFS = [0] #with no error this will hold the coefficients
        
        if design not in self.designs:
            print('Gave wrong filter design! Remember: butter, cheby1, cheby2.')
        elif filterType not in self.filterTypes1 and filterType not in self.filterTypes2:
            print('Gave wrong filter type! Remember: lowpass, highpass', 
                  ', bandpass, bandstop.')
        elif fs < 0:
            print('The sampling frequency has to be positive!')
        else:
            self.isThereAnError = 0
        
        #if fs was given then the given cutoffs need to be normalised to Nyquist
        if fs !=1:
            if isinstance(cutoff, float):
                cutoff/(fs*2)
            else:
                for i in range(len(cutoff)):
                    cutoff[i] = cutoff[i]/(fs*2)
        
        if design == 'butter' and self.isThereAnError == 0:
            self.COEFFS = signal.butter(order,cutoff,filterType,output='sos')
        elif design == 'cheby1' and self.isThereAnError == 0:
            self.COEFFS = signal.cheby1(order,rp,cutoff,filterType,output='sos')
        elif design == 'cheby2' and self.isThereAnError == 0:
            self.COEFFS = signal.cheby2(order,rs,cutoff,filterType,output='sos')
        elif design == 'bessel' and self.isThereAnError == 0:
            self.COEFFS = signal.bessel(order,cutoff,filterType,output='sos')
        
        return self.COEFFS
        
    def __init__(self,shape,order,cutoff,filterType='lowpass',design='bessel',device='cpu',rp=0.1,rs=1,fs=1):
        super().__init__()
        if isinstance(device, str): device = torch.device(device)
        self.device = device
        self.COEFFS = self.createCoeffs(order,cutoff,filterType,design,rp,rs,fs)
        self.shape = shape
        self.reset()
        
    def reset(self):
        self.sos_xz1 = [torch.zeros(self.shape).to(self.device) for _ in self.COEFFS]
        self.sos_xz2 = [torch.zeros(self.shape).to(self.device) for _ in self.COEFFS]
        self.sos_yz1 = [torch.zeros(self.shape).to(self.device) for _ in self.COEFFS]
        self.sos_yz2 = [torch.zeros(self.shape).to(self.device) for _ in self.COEFFS]
        
        torch.cuda.empty_cache()
        gc.collect()
       
    def forward(self,input):
        prev_stage_output = input
        
        for i in range(len(self.COEFFS)):
            
            numerators = self.COEFFS[i][0:3]
            denominators = self.COEFFS[i][3:6]
            
            output = numerators[0] * prev_stage_output + numerators[1] * self.sos_xz1[i] + numerators[2] * self.sos_xz2[i] - (denominators[1] * self.sos_yz1[i] + denominators[2] * self.sos_yz2[i])
            
            self.sos_xz2[i] = self.sos_xz1[i]
            self.sos_xz1[i] = prev_stage_output
            
            self.sos_yz2[i] = self.sos_yz1[i]
            self.sos_yz1[i] = output
            
            prev_stage_output = output
        
        return prev_stage_output
    