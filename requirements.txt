tensorflow-gpu==1.15.0
tensorflow-probability==0.8.0
qutip >= 4.2.0
scipy==1.2.1
numpy==1.16.3
comet-ml==2.0.18
tqdm==4.36.1