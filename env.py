from utils.upsampler import *
import math
from scipy.special import mathieu_a
import qutip
from qutip import operators
import numpy as np
import numpy.linalg
import scipy.sparse as sparse
import torch
from torch import nn
import torch.nn.functional as F
# from torchdiffeq import odeint_adjoint as odeint
from torchdiffeq import odeint

def kron(a, b):
    if a.ndim == b.ndim == 2: return torch.einsum("ab,cd->acbd", a, b).view(a.size(0)*b.size(0),  a.size(1)*b.size(1))
    elif a.ndim == b.ndim == 3: return torch.einsum("iab,icd->iacbd", a, b).view(a.size(0), a.size(1)*b.size(1),  a.size(2)*b.size(2))
    raise Exception('Shapes mismatch', a.shape, b.shape)
    


def mm(a,b):
    if a.ndim == b.ndim == 2: return a.mm(b)
    elif a.ndim == b.ndim == 3: return torch.einsum('bhw,bwk->bhk',a,b)
    raise Exception('Shapes mismatch', a.shape, b.shape)

def commut(a,b):
    return mm(a,b)-mm(b,a)
    
def get_fidelity(state_1, state_2):
        state_1_sqrt = state_1.sqrt()
        fid = mm(mm(state_1_sqrt, state_2), state_1_sqrt)
        eig_vals = torch.stack([torch.eig(x, eigenvectors=False)[0][:,0] for x in torch.unbind(fid, 0)])
#         eig_vals, eig_vect = torch.eig(F, eigenvectors=False)
#        eig_vals = tf.math.real(eig_vals)
        eig_vals_sqrt = F.relu(eig_vals).sqrt()
        return eig_vals_sqrt.sum(1)

# physical constants
# plank constant
hplank = 6.626e-34
# reduced plank constant
hbar = hplank / 2 / math.pi
# electron charge
e = 1.6e-19
# magnetic flux quantum
Fi0 = hplank / 2 / e
GHz = 1e9

cattet = lambda x: x + 1 - (x + 1) % 2
Ek_func = lambda x, Ec, Ej: mathieu_a(cattet(x), -2 * Ej / Ec) * Ec / 4

class Qubit(object):
    def __init__(self, Cq=1.01e-13, Cx=1e-16, Cg=1e-14, Ic=30e-9):
        self.Cx = Cx
        self.Cq = Cq
        self.Cg = Cg
        self.C = self.Cq + self.Cx + self.Cg
        self.Ic0 = 30e-9
        
        self.MeshDev = 100
        
    @property
    def Ec(self):
        return ((1 * e)**2 / (2 * self.C)) / (hplank*1e9) # GHz 
    
    def Ic(self, extFlux):
        return  self.Ic0 * abs(math.cos(math.pi*extFlux))
        
    def Ej(self, extFlux):
        return (2 * self.Ic(extFlux) * Fi0 / 2) / (hplank*1e9) # GHz
        
    def calcQubitSpectrum(self, extFlux, extVoltage):
        ng = extVoltage * self.Cx / (2 * e)
        phi = np.linspace(-math.pi, math.pi, self.MeshDev+1)
        phi = phi[0 : -1]
        step = 2*math.pi/self.MeshDev

        def alpha(phi):
            return -4*self.Ec

        def beta(phi):
            return 0

        def gamma(phi):
           return -math.cos(phi)*self.Ej(extFlux)

        diagCentr = np.zeros([self.MeshDev], dtype='complex')
        diagUp = np.zeros([self.MeshDev], dtype='complex')
        diagDown = np.zeros([self.MeshDev], dtype='complex')    

        for i in range(self.MeshDev):
            diagCentr[i] = gamma(phi[i])  - 2*alpha(phi[i])/(step*step)
            diagUp[i] = alpha(phi[i])/(step*step) + beta(phi[i])/2/step
            diagDown[i] = alpha(phi[i])/(step*step) - beta(phi[i])/2/step
                    
        phasefactor = np.exp(1j*ng*math.pi)   
        sm = sparse.diags([[np.conj(phasefactor)*diagUp[-1]], diagDown[1:], diagCentr, diagUp[0: -1], [phasefactor*diagDown[1]]], [-self.MeshDev + 1, -1, 0, 1, self.MeshDev -1])
        sm = sm.toarray();
        (el, evsol) = np.linalg.eigh(sm)
        return el, phi, evsol       

class EvolutionFunc(nn.Module):
    def __init__(self, qc, control_stategy):
        super(EvolutionFunc, self).__init__()
        self.qc = qc
        self.control_strategy = control_stategy

    def forward(self, t, rho):
        control = self.control_strategy(t)

#         if self.qc.debug:
#             qubit_state = self.qc.ptrace(rho, 1)
#             fidelity = get_fidelity(self.qc.qubit_target, qubit_state)
#             self.qc.qubit_state_history.append(qubit_state.numpy())
#             self.qc.fidelity_history.append(fidelity.numpy())
        rho_real, rho_imag = torch.unbind(rho, -1)
        
        tile = self.qc.Ht.shape[1:]
#         control = tf.transpose(tf.reshape(tf.tile(control,[tile[0]*tile[1]]), (tile[0],tile[1],self.qc.batch_size)))
        control = control.repeat(tile[0]*tile[1]).view(tile[0],tile[1],self.qc.batch_size).permute(2,1,0)
        
        H = self.qc.H + control * self.qc.Ht
        
        res_real, res_imag = commut(H,rho_imag), commut(rho_real,H)
        
        for c in self.qc.cOps:
            res_real += 0.5 * (2*mm(mm(c, rho_real), c.permute([0,2,1]))-\
                         mm(mm(rho_real, c.permute([0,2,1])), c)-\
                          mm(mm(c.permute([0,2,1]), c), rho_real))
            res_imag += 0.5 * (2*mm(mm(c, rho_imag), c.permute([0,2,1]))-\
                         mm(mm(rho_imag, c.permute([0,2,1])), c)-\
                          mm(mm(c.permute([0,2,1]), c), rho_imag))
#         print(res_real.mean(), res_imag.mean())
        return torch.stack([res_real,res_imag],-1)

        
class QuantumControl:
    def __init__(self, batch_size, device='cpu', 
                 nmax=2, Q=1e6, T1=1e3,
                 T2=1e3,
                 Zr=50, extFlux=0., 
                 extVoltage=0., omega=6, 
                 max_stamp=50,
                 RWA=False, temp=0.0, 
                 fock_space_size=12, 
                 use_exact_energy_levels=True,
                 f_c=11.198930e9, time_discount=False,
                 amp=1, reward_scaling=1,
                 if_timestep=1,
                 num_integration_steps=1000,debug=False,
                 inverse=False):
        if isinstance(device, str): self.device = torch.device(device)
        else: self.device = device
        self.f_c = f_c / GHz
        self.delta_time = if_timestep#GHz / f_c # nanoseconds
        self.amp = amp
        self.qubit = Qubit()
        self.reward_scaling=reward_scaling
        self.nmax = nmax
        self.fock_space_size = fock_space_size
        self.Zr = Zr
        self.extFlux = extFlux
        self.extVoltage = extVoltage
        self.temp = temp
        self.time_discount = time_discount
        n_actions = 2
        self.integration_step = self.delta_time / num_integration_steps
        cutoff = 1.0/(4*num_integration_steps) # 4 factor is used since 4th order Runge-Kutta is used below
        order = 5
        self.upsampler = IIR2Filter((batch_size,n_actions),order,cutoff,design='cheby1',device=device,filterType='lowpass',fs=1)
        self.batch_size = batch_size
        self.num_integration_steps = num_integration_steps

        
        # frequency of unloaded resonator
        self.omega = omega
        # it's Q-factor
        self.Q = Q
        # resonator relaxation parameter
        self.gamma = self.omega * 0.69 / self.Q

                # qubit energy relaxation time
        self.T1 = T1
                # qubit dephasing time
        self.T2 = T2

        # effective lumped capacitance of resonator
        self.Cr = 1 / (self.omega * self.Zr * GHz)
        
        C = self.qubit.Cq + self.qubit.Cg + self.qubit.Cx
        Ic =  self.qubit.Ic(self.extFlux)
        Ej = (Ic * Fi0) / (hplank * GHz)
        Ec = (e**2 / (2 * C)) / (hplank * GHz)
        ng = self.extVoltage * self.qubit.Cx / (2 * e) # for now assume that extVoltage == 0
        #energy_levels = [Ek_func(i * 2, Ec, Ej) for i in range(self.fock_space_size)[::-1]]
        energy_levels, _, _ = self.qubit.calcQubitSpectrum(self.extFlux, self.extVoltage)

            
        energy_levels = torch.Tensor(energy_levels[0:self.fock_space_size])
        

        # energy difference between first and second energy level of qubit
        self.epsilon = energy_levels[1] - energy_levels[0]# self._init_transition_energy()
        # print('Epsilon', self.epsilon)

        # qubit-resonator couping constant
        self.g = self.qubit.Cg / (2 * math.sqrt(self.qubit.Cq * self.Cr)) * math.sqrt(self.omega * self.epsilon)
        # print('g', self.g)

        self._init_operators()
        
        energy_levels = energy_levels - energy_levels[0]
        
        if use_exact_energy_levels:
            H_transmon = torch.diag(energy_levels)
            self.H_transmon = kron(torch.eye(self.nmax), H_transmon)
            self.H = 2 * math.pi * self.omega * mm(self.Aplus, self.Aminus) + 2 * math.pi * self.H_transmon
            # print(2 * pi * self.H_transmon)
        else:
            self.H = 2 * math.pi * self.omega * mm(self.Aplus, self.Aminus) +  2 * math.pi * 0.5 * self.epsilon * self.SigmaZ # + self.Qeye * (-19.11)
            # print(2 * pi * 0.5 * self.epsilon * self.SigmaZ)
        # interaction term
        # rabi model in rotating-wave approximation
        if RWA:
            self.H += self.g * (mm(self.Aminus,self.SigmaP) + mm(self.Aplus, self.SigmaN))
        # full hamiltonian with both Jaynes-Cummings (JC) and anti-Jaynes-Cummings (AJC) terms
        else:
            self.H += self.g * mm(self.Aminus + self.Aplus, self.SigmaX)
            
        # hamiltonian part representing qubit gate drive
        self.Ht = (e / (GHz * hplank)) * self.qubit.Cx * (self.qubit.Cq + self.qubit.Cg) / (self.qubit.Cq * self.qubit.Cg) * self.SigmaX
        
        # collapse operators
        self.cOps = [self.SigmaP * math.sqrt(self.temp / self.epsilon / self.T1),
                     self.SigmaN * math.sqrt((1 + self.temp / self.epsilon) / self.T1),
                     self.SigmaZ * math.sqrt(1 / self.epsilon / self.T2 / 2),
                     self.Aminus * math.sqrt(self.gamma * (1 + self.temp / self.omega)),
                     self.Aplus * math.sqrt(self.gamma * self.temp / self.omega)]
        
        
        zero_state, one_state = torch.Tensor(qutip.fock_dm(self.fock_space_size, 0).data.todense().real), \
            torch.Tensor(qutip.fock_dm(self.fock_space_size, 1).data.todense().real)

        if not inverse: qubit_start, qubit_target = zero_state, one_state
        else: qubit_start, qubit_target = one_state, zero_state
        
        # initial state of resonator in density matrix representation
        resonator_start = torch.Tensor(qutip.fock_dm(self.nmax, 0).data.todense().real)

        # define batch state
        self.qubit_start = torch.stack([qubit_start] * batch_size)
        self.resonator_start = torch.stack([resonator_start] * batch_size)
        self.qubit_target = torch.stack([qubit_target] * batch_size).double()
#        self.H = torch.stack([self.H] * batch_size)
#        self.Ht = torch.stack([self.Ht] * batch_size)
        
        self.qubit_resonator_start = kron(self.resonator_start, self.qubit_start)

        # Convert to complex (last axis)
        self.qubit_state = torch.stack([self.qubit_start.clone().detach(), torch.zeros_like(self.qubit_start)], -1) # concat real and imag parts
        self.resonator_state = torch.stack([self.resonator_start.clone().detach(), torch.zeros_like(self.resonator_start)], -1) # concat real and imag parts
        self.qubit_resonator_state = kron(self.resonator_start, self.qubit_start)
        self.qubit_resonator_state = torch.stack([self.qubit_resonator_state, torch.zeros_like(self.qubit_resonator_state)], -1) # concat real and imag parts
        self.qubit_resonator_state_shape = (self.resonator_start.shape[1], self.qubit_start.shape[1])
        self.time_stamp = 0
        self.time_stamp_start = torch.Tensor([0.0]*batch_size)
        self.fidelity_start = torch.Tensor([0.0]*batch_size)
        self.max_stamp = max_stamp

        self.debug = debug
        if self.debug:
            self.actions_history = []
            self.gate_drive_history = []
            self.fidelity_history = []
            self.qubit_state_history = []

        self.seed()
        self.reset()
    
    def ptrace(self, qrstate, axis):
        res = []
        def trace(tensor):
            return torch.einsum('abcdd->abc',tensor)
        for qubit_resonator_state in torch.unbind(qrstate,-1):
            shape_0, shape_1 = self.qubit_resonator_state_shape
            state_reshaped = qubit_resonator_state.view(self.batch_size, shape_0,shape_1,shape_0,shape_1)
            if axis == 0: res.append(trace(state_reshaped.permute([0,1,3,2,4])))
            else: res.append(trace(state_reshaped.permute([0,2,4,1,3])))
        return torch.stack(res,-1)
        
        
    
    def _init_operators(self):
        # lc resonator creation operator
        self.Aplus = kron(torch.Tensor(qutip.create(self.nmax).data.todense().real), 
                               torch.eye(self.fock_space_size)).double()
        # annihilation operators
        self.Aminus = kron(torch.Tensor(qutip.destroy(self.nmax).data.todense().real), 
                              torch.eye(self.fock_space_size)).double()
        
        #self.sigmax = operators.jmat((self.fock_space_size - 1) / 2, 'x') * 2
        #self.sigmaz = operators.jmat((self.fock_space_size - 1) / 2, 'z') * 2
        #self.sigmap = operators.jmat((self.fock_space_size - 1) / 2, '+')
        #self.sigmam = operators.jmat((self.fock_space_size - 1) / 2, '-')
        
        self.sigmap = torch.Tensor(operators.create(self.fock_space_size).data.todense().real).double()
        self.sigmam = torch.Tensor(operators.destroy(self.fock_space_size).data.todense().real).double()
        self.sigmax = self.sigmap + self.sigmam
        self.sigmaz = mm(self.sigmap, self.sigmam)
        
        self.qubit_projectors = []
        self.QubitProjectors = []
        
        for i in range(self.fock_space_size):
            diag = [0 for _ in range(self.fock_space_size)]
            diag[i] = 1
            diag = torch.Tensor(diag)
            projector = torch.diag(diag).double();
            self.qubit_projectors.append(projector)
            
            self.QubitProjectors.append(kron(torch.eye(self.nmax).double(), projector))

        # pauli sigma matrix at x direction
        self.SigmaX = kron(torch.eye(self.nmax), self.sigmax).double()
        # pauli sigma matrix at z direction
        self.SigmaZ = kron(torch.eye(self.nmax), self.sigmaz).double()
        # creation operator
        self.SigmaP = kron(torch.eye(self.nmax), self.sigmap).double()
        # annihilation operator
        self.SigmaN = kron(torch.eye(self.nmax), self.sigmam).double()
        # particle number operator
        self.SigmaPopulation = kron(torch.eye(self.nmax), mm(self.sigmap, self.sigmam)).double()
        self.Qeye = kron(torch.eye(self.nmax), torch.eye(self.fock_space_size)).double()

        # batch operators
        self.Aplus = torch.stack([self.Aplus] * self.batch_size).double()
        self.Aminus = torch.stack([self.Aminus] * self.batch_size).double()
        self.SigmaX = torch.stack([self.SigmaX] * self.batch_size).double()
        self.SigmaN = torch.stack([self.SigmaN] * self.batch_size).double()
        self.SigmaPopulation = torch.stack([self.SigmaPopulation] * self.batch_size).double()
        self.SigmaP = torch.stack([self.SigmaP] * self.batch_size).double()
        self.SigmaZ = torch.stack([self.SigmaZ] * self.batch_size).double()
        
        
    def _init_transition_energy(self):
        """
        Calculate energy difference between zero level and first level of qubit
        """
        # (ev, phis, evec) = self.__qubitSpectrum(self.qp, 100, self.extFlux, self.extVoltage)
        Ic = self.qubit.Ic * abs(math.cos(math.pi * self.extFlux))
        Ej = (2 * Ic * Fi0 / 2) / (hplank * GHz)
        Ec = (e ** 2 / (2 * self.qubit.C)) / (hplank * GHz)
        return math.sqrt(8 * Ec * Ej) - Ec

        
    def reset(self):
        self.qubit_state = self.qubit_start.clone().detach().double()
        self.resonator_state = self.resonator_start.clone().detach().double()
        self.time_stamp = self.time_stamp_start.clone().detach().double()
        self.fidelity = self.fidelity_start.clone().detach().double()
        self.upsampler.reset()
        if self.debug:
            self.actions_history = []
            self.gate_drive_history = []
            self.fidelity_history = []
            self.qubit_state_history = []
        
            
        self.qubit_resonator_state = kron(self.resonator_state, self.qubit_state)
    
        # Convert to complex
        self.qubit_state = torch.stack([self.qubit_state, torch.zeros_like(self.qubit_state)],-1).double()
        self.resonator_state = torch.stack([self.resonator_state, torch.zeros_like(self.resonator_state)],-1).double()
        self.qubit_resonator_state = torch.stack([self.qubit_resonator_state, torch.zeros_like(self.qubit_resonator_state)],-1).double()
        self.qubit_resonator_state.requires_grad = True
        self.qubit_state.requires_grad = True
        self.resonator_state.requires_grad = True

        return self.eval_obs(self.qubit_resonator_state, self.fidelity)

#        return self.step(tf.constant(0.0,dtype=tf.float64), tf.constant(0.0,dtype=tf.float64))[0] # take 0.0 action

    def update_initial_state(self):
        self.qubit_start = self.qubit_state.clone().detach()
        self.resonator_start = self.resonator_state.clone().detach()
        self.time_stamp_start = self.time_stamp.clone().detach()
        self.fidelity_start = self.fidelity.clone().detach()


#   def dump(self, f):
#       np.savez(f, self.qubit_state, self.resonator_state, self.time_stamp, self.fidelity)

#   def load(self, f):
#       dump = np.load(f)
#       self.qubit_state, self.resonator_state, self.time_stamp, self.fidelity = tf.constant(dump['arr_0'][:self.batch_size]), \
#           tf.constant(dump['arr_1'][:self.batch_size]), tf.constant(dump['arr_2'][:self.batch_size]), tf.constant(dump['arr_3'][:self.batch_size])
#       self.qubit_resonator_state = kron(self.resonator_state, self.qubit_state)

    def seed(self, seed=None):
        if not seed is None: torch.seed(seed)
    
    
    def __state_data(self):
        return torch.cat([x.view(-1) for x in torch.unbind(self.qubit_state,-1)], 0)

    def _optimal_control(self, t, args, action):
        action = self.upsampler(action)
        
        if self.debug:
            self.actions_history.append(action.detach.cpu().numpy())
        self.first_call = False
        
        gate_drive = action[:, 0] * args['amp'] * torch.sin(2 * math.pi * args['f_c'] * (args['time'] + t)) + \
               action[:, 1] * args['amp'] * torch.cos(2 * math.pi * args['f_c'] * (args['time'] + t))
        
        
        if(self.debug):
            self.gate_drive_history.append(gate_drive.detach().cpu().numpy())
            
        return gate_drive

    @staticmethod
    def _basic_control(t,args,action):
        return args['amp'] * action[:, 0]

#    @staticmethod
#    def _optimal_control(t, args, action):
#        return args['amp'] * action
    
    def _qubit_eval(self, action, qubit_resonator_state, ts_delta):
        
        time = (self.time_stamp + torch.ones_like(self.time_stamp) * ts_delta) * self.delta_time
        delta_time = self.delta_time
        #if self.__subdiv_ts > 1:   # WTF is this thing?
        #   time = 2*(time + self.__subdiv_ts*self.delta_time/(self.control_upsampling))
        #   delta_time = self.delta_time / (self.control_upsampling )
        
        args={'f_c': self.f_c, 
              'time': time, 'amp': self.amp}
        
        # control_strategy = self._basic_control
        
        control_strategy = lambda t: self._optimal_control(t,args,action)
        
        qubit_resonator_state_next = odeint(EvolutionFunc(self, control_strategy), qubit_resonator_state, 
                                            torch.Tensor([0,delta_time]),method='rk4',options={'step_size':self.integration_step})[-1]
        qubit_state_next = self.ptrace(qubit_resonator_state_next, 1)
        resonator_state_next = self.ptrace(qubit_resonator_state_next, 0)
        fidelity_next = get_fidelity(self.qubit_target, torch.unbind(qubit_state_next,-1)[0])
        obs_next = self.eval_obs(qubit_resonator_state_next, fidelity_next)

        return fidelity_next, qubit_state_next, resonator_state_next, qubit_resonator_state_next, obs_next, time


    def eval_next_state(self, action, qubit_resonator_state, ts_delta=0):
        self.first_call = True
        new_fidelity, qubit_state, resonator_state, qubit_resonator_state, observable_data, time = self._qubit_eval(action, 
            qubit_resonator_state, ts_delta)
        return new_fidelity, qubit_state, resonator_state, qubit_resonator_state, observable_data, time


    def eval_obs(self, qubit_resonator_state, fidelity):
        observable_data = []
        state_real, _ = torch.unbind(qubit_resonator_state, -1)
        for operator in [self.Aplus + self.Aminus,
                                mm(self.Aplus, self.Aminus),
                                self.SigmaZ,
                                self.SigmaX,
                                self.SigmaPopulation]:
            obs = (state_real * operator).sum((1,2))
            observable_data.append(obs)
        observable_data.append(fidelity)
        return torch.stack(observable_data).T
    

    def step(self, action):
        new_fidelity, qubit_state, resonator_state, qubit_resonator_state, observable_data, time = self.eval_next_state(action, self.qubit_resonator_state, 0)
        
        self.time_stamp += 1
        episode_over = (self.time_stamp >= self.max_stamp) | (new_fidelity >= 1)

        
        self.qubit_state = qubit_state
        self.resonator_state = resonator_state
        self.qubit_resonator_state = qubit_resonator_state
                        
        reward = new_fidelity - self.fidelity
        self.fidelity = new_fidelity
        
        reward = self.reward_scaling * reward
        if self.time_discount: reward /= sqrt(self.time_stamp)
                    
        return observable_data, reward, episode_over, self.fidelity

    def apply(self, actions):
        for action in actions:
            self.step(torch.Tensor([action]*self.batch_size))
